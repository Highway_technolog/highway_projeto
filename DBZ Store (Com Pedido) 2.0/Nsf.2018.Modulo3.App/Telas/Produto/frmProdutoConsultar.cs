﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.Modulo3.App.DB.Produto;

namespace Nsf._2018.Modulo3.App.Telas
{
    public partial class frmProdutoConsultar : UserControl
    {
        public frmProdutoConsultar()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            CarregarGrid();
        
        }

        public void CarregarGrid()

        { 
            ProdutoBusiness business = new ProdutoBusiness();
            List<ProdutoDTO> lista = business.Consultar(txtProduto.Text);

            dgvProdutos.AutoGenerateColumns = false;
            dgvProdutos.DataSource = lista;
        }

        private void dgvProdutos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 3)
            {
                ProdutoDTO produto = dgvProdutos.CurrentRow.DataBoundItem as ProdutoDTO;

                DialogResult r = MessageBox.Show("Deseja excluir este produto?", " Nsf Jam",
                                                    MessageBoxButtons.YesNo,
                                                    MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                    {
                    ProdutoBusiness business = new ProdutoBusiness();
                    business.Remover(produto.Id);

                    CarregarGrid();


                    }


            }
        }
    }
}

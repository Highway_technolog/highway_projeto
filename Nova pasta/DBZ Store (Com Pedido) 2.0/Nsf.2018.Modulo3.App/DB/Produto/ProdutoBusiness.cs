﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Produto
{
     class ProdutoBusiness
    {
        public int Salvar(ProdutoDTO dto)
        {
            ProdutoDatabase db = new ProdutoDatabase();
            return db.Salvar(dto);
        }

        public void Alterar(ProdutoDTO dto)
        {
            ProdutoDatabase db = new ProdutoDatabase();
            db.Alterar(dto);
        }

        public void Remover(int id)
        {
            ProdutoDatabase db = new ProdutoDatabase();
            db.Remover(id);
        }

        public List<ProdutoDTO> Consultar(string produto)
        {
            ProdutoDatabase db = new ProdutoDatabase();
            return db.Consultar(produto);
        }

        public List<ProdutoDTO> Listar()
        {
            ProdutoDatabase db = new ProdutoDatabase();
            return db.Listar();
        }



    }
}

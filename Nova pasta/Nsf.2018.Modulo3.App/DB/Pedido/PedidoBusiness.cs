﻿using Nsf._2018.Modulo3.App.DB.Base.coisinhas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Pedido
{
    class PedidoBusiness
    {

        public int Salvar(PedidoDTO pedido, List<ProdutoDTO> produtos)
        {
            PedidoDataBase pedidoDatabase = new PedidoDataBase();
            int idPedido = pedidoDatabase.Salvar(pedido);

            PedidoItemBusiness itemBusiness = new PedidoItemBusiness();
            foreach (ProdutoDTO item in produtos)
            {
                PedidoItemDTO itemDto = new PedidoItemDTO();
                itemDto.IdPedido = idPedido;
                itemDto.IdProduto = item.Id;

                itemBusiness.Salvar(itemDto);
            }

            return idPedido;
        }




        public List<PedidoConsultarView> Consultar(string cliente)
        {
            PedidoDataBase pedidoDatabase = new PedidoDataBase();
            return pedidoDatabase.Consultar(cliente);
        }


    }
}

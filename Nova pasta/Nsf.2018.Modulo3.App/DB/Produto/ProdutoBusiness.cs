﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Base.coisinhas
{
    class ProdutoBusiness
    {
        ProdutoDataBase db = new ProdutoDataBase();

        public int Salvar(ProdutoDTO drag)
        {
            if (drag.Produto == string.Empty)
            {
                throw new ArgumentException("É obrigatório o nome do produto.");
            }



            if (drag.Preco == 0)
            {
                throw new ArgumentException("É obrigatório colocar um valor.");

            }


            return db.Salvar(drag);
        }

        public List<ProdutoDTO> Consultar(string produto)
        {


            if (produto == "todos")
            {
                produto = string.Empty;



            }



            return db.Consultar(produto);
        }

        public void Alterar(ProdutoDTO dto)
        {
            ProdutoDataBase db = new ProdutoDataBase();
            db.Alterar(dto);

           

        }

        public void Remover(int id)
        {
            ProdutoDataBase db = new ProdutoDataBase();
            db.Remover(id);
        }


        public List<ProdutoDTO> Listar()
        {
            ProdutoDataBase db = new ProdutoDataBase();
            return db.Listar();
        }




    }

}

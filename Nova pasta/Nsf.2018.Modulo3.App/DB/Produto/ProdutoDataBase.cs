﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Base.coisinhas
{
    public class ProdutoDataBase
    {
        public int Salvar(ProdutoDTO drag)
        {
            string script =
                @"INSERT INTO tb_produto
                (
                  nm_produto,
                  vl_preco
                )
                VALUES
                (
                  @nm_produto,
                  @vl_preco

                )";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", drag.Produto));
            parms.Add(new MySqlParameter("vl_preco", drag.Preco));

            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;



        }


        public List<ProdutoDTO> Consultar(string produto)
        {
            string script =
            @"SELECT *
                 FROM tb_produto
                WHERE nm_produto like @nm_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", produto));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProdutoDTO> drag = new List<ProdutoDTO>();

            while (reader.Read())
            {
                ProdutoDTO novoproduto = new ProdutoDTO();
                novoproduto.Id = reader.GetInt32("id_produto");
                novoproduto.Produto = reader.GetString("nm_produto");
                novoproduto.Preco = reader.GetDecimal("vl_preco");

                drag.Add(novoproduto);

            }
            reader.Close();

            return drag;
        }
        public void Alterar(ProdutoDTO dto)
        {
            string script = @"UPDATE tb_produto 
                                 SET nm_produto = @nm_produto,
                                     vl_preco   = @vl_preco
                               WHERE id_produto = @id_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", dto.Id));
            parms.Add(new MySqlParameter("nm_produto", dto.Produto));
            parms.Add(new MySqlParameter("vl_preco", dto.Preco));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);



        }



        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_produto WHERE id_produto = @id_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }


        public List<ProdutoDTO> Listar()
        {
            string script = @"SELECT * FROM tb_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProdutoDTO> lista = new List<ProdutoDTO>();
            while (reader.Read())
            {
                ProdutoDTO dto = new ProdutoDTO();
                dto.Id = reader.GetInt32("id_produto");
                dto.Produto = reader.GetString("nm_produto");
                dto.Preco = reader.GetDecimal("vl_preco");

                lista.Add(dto);
            }
            reader.Close();

            return lista;












        }

    }
}

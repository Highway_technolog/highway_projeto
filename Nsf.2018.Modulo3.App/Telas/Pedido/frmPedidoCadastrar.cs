﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.Modulo3.App.DB.Pedido;
using Nsf._2018.Modulo3.App.DB.Base.coisinhas;

namespace Nsf._2018.Modulo3.App.Telas
{
    public partial class frmPedidoCadastrar : UserControl
    {

        BindingList<ProdutoDTO> produtosCarrinho = new BindingList<ProdutoDTO>();

        public frmPedidoCadastrar()
        {
            InitializeComponent();
            CarregarCombos();
            ConfigurarGrid();
        }

        void CarregarCombos()
        {
            ProdutoBusiness business = new ProdutoBusiness();
            List<ProdutoDTO> lista = business.Listar();

            cboProduto.ValueMember = nameof(ProdutoDTO.Id);
            cboProduto.DisplayMember = nameof(ProdutoDTO.Produto);
            cboProduto.DataSource = lista;
        }

        void ConfigurarGrid()
        {
            dgvItens.AutoGenerateColumns = false;
            dgvItens.DataSource = produtosCarrinho;
        }

        private void btnEmitir_Click(object sender, EventArgs e)
        {
            PedidoDTO dto = new PedidoDTO();
            dto.Cliente = txtCliente.Text;
            dto.Cpf = txtCpf.Text;
            dto.Data = DateTime.Now;

            PedidoBusiness business = new PedidoBusiness();
            business.Salvar(dto, produtosCarrinho.ToList());

            MessageBox.Show("Pedido salvo com sucesso.", "DBZ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Hide();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            ProdutoDTO dto = cboProduto.SelectedItem as ProdutoDTO;

            for (int i = 0; i < int.Parse(txtQuantidade.Text); i++)
            {
                produtosCarrinho.Add(dto);
            }

        }

        private void dgvItens_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void cboProduto_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txtCliente_TextChanged(object sender, EventArgs e)
        {

        }




    }
}
